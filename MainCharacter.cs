using UnityEngine;
using System.Collections;

public class MainCharacter : MonoBehaviour {
	
	bool canMove = true;
	bool gotFirstValue = false;
	Quaternion initialSensorValue;
	
	Quaternion differenceRotation;
	Vector3 differenceEuler;
	
	public float rotationStrength = 1;
	public float movementStrength = 10;
	public float sensitivity = 2.0f;
	public float rotationSensitivity = 2.0f;
	public bool showGUI = false;
	
	// Mecanim animation
	public Animator anim;
	public float animationSpeed = 1.5f;

	void Start() {
		// Set mecanim animator.
		anim = GetComponent<Animator>();
		// Start gyrodroid controlls.
		SensorHelper.ActivateRotation();
		initialSensorValue = SensorHelper.rotation;
		gotFirstValue = false;
		// Setup controll callibration.
		StartCoroutine(Calibration());
	}
	
	void Update() {
		if(!gotFirstValue || !canMove) {
			return;
		}
		
		// calculate difference between current rotation and initial rotation
		differenceRotation = FromToRotation(initialSensorValue, SensorHelper.rotation);
		
		// differenceEuler is the difference in degrees between the current SensorHelper.rotation and the initial value
		differenceEuler = differenceRotation.eulerAngles;
		
		if(differenceEuler.x > 180) differenceEuler.x -= 360;
		if(differenceEuler.y > 180) differenceEuler.y -= 360;
		if(differenceEuler.z > 180) differenceEuler.z -= 360;
		
		// rotate us
		Vector3 rotationVector = differenceEuler;
		rotationVector.x = 0;
		rotationVector.z = 0;
		if(differenceEuler.z > rotationSensitivity || differenceEuler.z < -rotationSensitivity) {
			transform.Rotate(rotationVector * rotationStrength * Time.deltaTime);
			if(differenceEuler.z > sensitivity) {
				anim.SetFloat("direction", 1);
			} else if(differenceEuler.z < -sensitivity) {
				anim.SetFloat("direction", -1);
			}
			anim.speed = animationSpeed;
		}
		
		// for an airplane: disable yaw,
		// only use roll and pitch
		differenceEuler.y = 0;
		// move forward all the time (no speed control)
		if(differenceEuler.x > sensitivity) {
			transform.Translate(Vector3.forward * movementStrength * Time.deltaTime, Space.Self);
			anim.SetFloat("speed", 1);
		} else {
			anim.SetFloat("speed", 0);
		}
	}
	
	
	public void OnGUI() {
		if(!showGUI) {
			return;
		}
		GUI.Label(new Rect(10,10,200,25), "Relative rotation to start in degrees:");
		GUI.Label(new Rect(10,40,200,25), "" + differenceEuler);
		GUI.Label(new Rect(10,70,200,25), "" + differenceEuler.z);
	}
	
	/// <summary>
	/// Calculates the rotation C needed to rotate from A to B.
	/// </summary>
	public static Quaternion FromToRotation(Quaternion a, Quaternion b) {
		return Quaternion.Inverse(a) * b;
	}
	
	IEnumerator Calibration() {
		while(! SensorHelper.gotFirstValue) {
			SensorHelper.FetchValue();
			yield return null;
		}
		
		// wait some frames
		yield return new WaitForSeconds(0.1f);
		
		// set initial rotation
		initialSensorValue = SensorHelper.rotation;
		
		// allow updates
		gotFirstValue = true;
	}
	
	void EndGame() {
		canMove = false;
	}
}
