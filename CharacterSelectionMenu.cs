using UnityEngine;
using System.Collections;

public class CharacterSelectionMenu : MonoBehaviour {
	
	// GUI
	public GUISkin gSkin;
	
	// Use this for initialization
	void OnGUI () {
		GUI.skin = gSkin;
		Rect areaRect = new Rect(Screen.width / 2 - 200, Screen.height - 230, 400, 250);
		areaRect = GUI.Window(0, areaRect, BeginGameWindow, "");
		//now adjust to the group. (0,0) is the topleft corner of the group.
		GUI.BeginGroup(new Rect (0,0,100,100));
		// End the group we started above. This is very important to remember!
		GUI.EndGroup();
		
	}
	
	void StartGame() {
		string selectedCharacter = PlayerPrefs.GetString("SelectedCharacter");
		if(selectedCharacter == "") {
			CharacterSelection charSelect = gameObject.GetComponent<CharacterSelection>();
			PlayerPrefs.SetString("SelectedCharacter", charSelect.defaultCharacter);
		}
		Application.LoadLevel("Level01");
	}
	
	//bringing it all together
	void BeginGameWindow (int windowID) {
		// use the spike function to add the spikes
		// note: were passing the width of the window to the function
		//Rect areaRect = new Rect(Screen.width / 2 - 100, Screen.height / 2 + 120, 200, 100);
		//GUIHelper.AddSpikes(areaRect.width);

		GUILayout.BeginVertical();
		GUILayout.Space(8);
		GUILayout.Label("", "Divider");//-------------------------------- custom
        GUILayout.Label("Character selection");
		GUILayout.Box("Select your character and begin your trials.");
		GUILayout.Label("", "Divider");//-------------------------------- custom
		if(GUILayout.Button("Begin game")) {
			StartGame();
		}
		/*GUILayout.Label("", "Divider");//-------------------------------- custom
		ToggleBTN = GUILayout.Toggle(ToggleBTN, "This is a Toggle Button");
		GUILayout.Label("", "Divider");//-------------------------------- custom
		GUILayout.Box("This is a textbox\n this can be expanded by using \\n");
		GUILayout.TextField("This is a textfield\n You cant see this text!!");
        GUILayout.TextArea("This is a textArea\n this can be expanded by using \\n");*/
		GUILayout.EndVertical();
		
		// Make the windows be draggable.
		//GUI.DragWindow (Rect (0,0,10000,10000));
	}
}
