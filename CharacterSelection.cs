/**
 * Inspired by James Arndt : http://www.youtube.com/user/PolyNurb?feature=watch
 */

using UnityEngine;
using System.Collections;

public class CharacterSelection : MonoBehaviour {
	
	public string defaultCharacter = "CharacterTwo";

	// Use this for initialization
	void Awake () {
		DisableParticles();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonUp(0)) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if(Physics.Raycast(ray, out hit, 100)) {
				switch(hit.collider.name) {
				case "CharacterOneSelection":
					SelectCharacter("CharacterOne");
					break;
				default:
					SelectCharacter(defaultCharacter);
					break;
				}
			} else {
				return;
			}
		}
	}
	
	void DisableParticles() {
		GameObject[] characters = GameObject.FindGameObjectsWithTag("Player");
		foreach(GameObject character in characters) {
			ParticleSystem system = character.GetComponentInChildren(typeof(ParticleSystem)) as ParticleSystem;
			if(system) {
				system.renderer.enabled = false;
			}
		}
	}
	
	void SelectCharacter(string character) {
		PlayerPrefs.SetString("SelectedCharacter", character);
		DisableParticles();
		GameObject selected = GameObject.Find(character + "Selection");
		selected.GetComponentInChildren(typeof(ParticleSystem)).renderer.enabled = true;
	}
}
