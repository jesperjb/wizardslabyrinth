using UnityEngine;
using System.Collections;

public class SelectLevel : MonoBehaviour {
	
	// Game manager object
	private GameManager controller;
	
	// Game settings
	private int firstLevel;
	private int levelCount;
	public int horizontalLevels = 4;
	
	// GUI
	public GUISkin gSkin;
	
	void Awake () {
		if(!controller) {
			controller = GameObject.FindObjectOfType(typeof(GameManager)) as GameManager;
			firstLevel = controller.firstLevel;
			levelCount = controller.levelCount;
		}
	}
	
	// Use this for initialization
	void OnGUI () {
		GUI.skin = gSkin;
		Rect menuRect = new Rect(Screen.width / 2 - 200, Screen.height / 2 - 250, 400, 500);
		menuRect = GUI.Window(0, menuRect, SelectLevelWindow, "");
		//now adjust to the group. (0,0) is the topleft corner of the group.
		GUI.BeginGroup(new Rect (0,0,100,100));
		// End the group we started above. This is very important to remember!
		GUI.EndGroup();
		/*
		GUI.skin = gSkin;
		int currentLevel = PlayerPrefs.GetInt("CurrentLevel", firstLevel);
		Rect menuRect = new Rect(Screen.width / 2 - 100, Screen.height / 2 - 100, 200, 200);
		GUILayout.BeginArea(menuRect);
		GUILayout.FlexibleSpace();
		for(int i = firstLevel; i < firstLevel + levelCount; i++) {
			if(GUILayout.Button("Load level " + (i - 1), "ShortButton") && currentLevel >= i) {
				controller.LoadLevel(i);
			}
		}
		GUILayout.FlexibleSpace();
        GUILayout.EndArea();
        */
	}
	
	//bringing it all together
	void SelectLevelWindow (int windowID) {
		GUILayout.BeginVertical();
		GUILayout.Space(8);
		GUILayout.Label("", "Divider");//-------------------------------- custom
        GUILayout.Label("Select level");
		GUILayout.Box("Choose the level you wish to try.\nYou can only select levels which you have reached so far.\nNew levels are opened as you progress.");
		GUILayout.Label("", "Divider");//-------------------------------- custom
		int currentLevel = PlayerPrefs.GetInt("CurrentLevel", firstLevel);
		for(int i = firstLevel; i < firstLevel + levelCount; i++) {
			/*if((i - firstLevel) % horizontalLevels == 0) {
				GUILayout.BeginHorizontal();
			}*/
			if(GUILayout.Button("Load level " + (i - 1), "ShortButton") && currentLevel >= i) {
				controller.LoadLevel(i);
			}
			/*if((i - firstLevel) % horizontalLevels == (horizontalLevels - 1) || i == (firstLevel + levelCount)) {
				GUILayout.EndHorizontal();
			}*/
		}
		GUILayout.EndVertical();
	}
	
}
