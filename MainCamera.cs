/**
 * Taken from Unity SmoothFollow and modified with inspiration by 
 * Trypho : http://forum.unity3d.com/threads/5989-Camera-collision-code-snippet?p=48699&viewfull=1#post48699
 * and Raiden : http://forum.unity3d.com/threads/5989-Camera-collision-code-snippet?p=208370&viewfull=1#post208370
 */

using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour {
	
	// The target we are following
	public Transform target;
	// The distance in the x-z plane to the target
	public float distance = 10.0f;
	// the height we want the camera to be above the target
	public float height = 5.0f;
	// Distance above player to opserve
	public float heightOffset = 2.0f;
	// How much we 
	public float heightDamping = 2.0f;
	public float rotationDamping = 3.0f;
	
	//how close does the camera have to get to a wall before it will bounce
	public float bounceRange = 0.5f;
	public float bounceAmt = 20;

	// Use this for initialization
	void Start () {
		// If target has not been set, try to find the MainCharacter and assign its transform to target.
		if(!target) {
			FindPlayer();
		}
	}
	
	void LateUpdate () {
		if(!target && !FindPlayer()) {
			return;
		}
		// Calculate the current rotation angles
		float wantedRotationAngle = target.eulerAngles.y;
		float wantedHeight = target.position.y + height;
			
		float currentRotationAngle = transform.eulerAngles.y;
		float currentHeight = transform.position.y;
		//Cast rays to the left and right of the camera, to detect walls.

	    RaycastHit hit;
		int layerMask = 1 << 11;
		layerMask = ~layerMask; // ignore the player (just to make sure)  
		if (Physics.Raycast(transform.position, transform.TransformDirection(-Vector3.right), out hit, bounceRange, layerMask)) {      
	      currentRotationAngle -= bounceAmt * Time.deltaTime;
		} else if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out hit, bounceRange, layerMask)) {
			currentRotationAngle += bounceAmt * Time.deltaTime;
		} else {
			//If no walls are detected, execute the normal follow behavior.
			// Damp the rotation around the y-axis 
			if (!Physics.CheckSphere(transform.position, 1.0f, layerMask)) {
		        currentRotationAngle = Mathf.LerpAngle (currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);
			}
		}
		// Damp the rotation around the y-axis
		//currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);
	
		// Damp the height
		currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);
	
		// Convert the angle into a rotation
		var currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);
		
		// Set the position of the camera on the x-z plane to:
		// distance meters behind the target
		transform.position = target.position;
		transform.position -= currentRotation * Vector3.forward * distance;
	
		// Set the height of the camera
		transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);
		
		// Always look at the target
		Vector3 newDirection = target.position;
		newDirection += new Vector3(0, heightOffset, 0);
		transform.LookAt(newDirection);
	}
	
	bool FindPlayer() {
		MainCharacter player = FindObjectOfType(typeof(MainCharacter)) as MainCharacter;
		if(player) {
			target = player.transform;
			return true;
		} else {
			return false;
		}
	}
}
