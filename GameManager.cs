using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	
	// The current level
	public int currentLevel = 0;
	// Game settings
	public int firstLevel = 2;
	public int levelCount = 2;
	// Show or hide level
	private bool displayMenu = false;
	public bool enableMenu = true;
	// GUI
	public GUISkin gSkin;

	// Use this for initialization
	void Awake () {
		if(currentLevel == 0) {
			currentLevel = Application.loadedLevel;
		}
	}
	
	void OnGUI() {
		GUI.skin = gSkin;
		if(enableMenu) {
			Rect areaRect = new Rect(10, Screen.height - 50, 100, 30);
			GUILayout.BeginArea(areaRect);
			GUILayout.FlexibleSpace();
			if(displayMenu) {
				if(GUILayout.Button("Hide menu")) {
					displayMenu = false;
				}
			} else {
				if(GUILayout.Button("Show menu")) {
					displayMenu = true;
				}
			}
			GUILayout.FlexibleSpace();
	        GUILayout.EndArea();
			if(displayMenu) {
				Rect menuRect = new Rect(Screen.width / 2 - 100, Screen.height / 2 - 100, 200, 200);
				GUILayout.BeginArea(menuRect);
				GUILayout.FlexibleSpace();
				if(GUILayout.Button("Restart")) {
					RestartLevel();
				}
				if(GUILayout.Button("Save")) {
					SaveLevel();
				}
				if(GUILayout.Button("Load")) {
					LoadSelectLevel();
				}
				GUILayout.FlexibleSpace();
		        GUILayout.EndArea();
			}
		}
	}
	
	public void LoadNextLevel() {
		Application.LoadLevel(Application.loadedLevel + 1);
	}
	
	public void RestartLevel() {
		Application.LoadLevel(Application.loadedLevel);
	}
	
	public void SaveLevel() {
		PlayerPrefs.SetInt("CurrentLevel", currentLevel);
	}
	
	public void LoadLevel(int levelNumber) {
		Application.LoadLevel(levelNumber);
	}

	public void LoadSelectLevel() {
		Application.LoadLevel("SelectLevel");
	}
}
