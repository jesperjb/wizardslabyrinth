using UnityEngine;
using System.Collections;

public class Map : MonoBehaviour {
	
	private Camera camera;
	private bool showMap = false;
	public Texture2D backgroundTexture;
	public Texture2D mapTexture;
	// GUI
	public GUISkin gSkin;
	
	// Use this for initialization
	void Start () {
		camera = Camera.main;
	}
	
	void OnGUI () {
		GUI.skin = gSkin;
		if(showMap) {
			Debug.Log(backgroundTexture.width + " : " + backgroundTexture.height);
			Debug.Log((Screen.width - 20) + " : " + (Screen.height - 20));
			GUI.DrawTexture(new Rect(10, 10, Screen.width - 20, Screen.height - 20), backgroundTexture, ScaleMode.ScaleToFit);
			GUI.DrawTexture(new Rect(10, 10, Screen.width - 20, Screen.height - 20), mapTexture, ScaleMode.ScaleToFit);
		}
		Rect areaRect = new Rect(Screen.width - 120, Screen.height - 50, 100, 30);
		GUILayout.BeginArea(areaRect);
		GUILayout.FlexibleSpace();
		if (showMap) {
			if(GUILayout.Button("Hide map")) {
				ShowMap();
			}
		} else {
			if(GUILayout.Button("Show map")) {
				ShowMap();
			}
		}
		GUILayout.FlexibleSpace();
        GUILayout.EndArea();
	}
	
	void ShowMap() {
		showMap = !showMap;
	}
}
