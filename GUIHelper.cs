using UnityEngine;
using System.Collections;

public class GUIHelper : MonoBehaviour {
	
	public static int spikeSeparation = 19;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public static void AddSpikes(float winX) {
		float spikeCount = Mathf.Floor(winX) / GUIHelper.spikeSeparation;
		GUILayout.BeginHorizontal();
		GUILayout.Label ("", "SpikeLeft");//-------------------------------- custom
		for (var i = 0; i < spikeCount; i++)
	        {
				GUILayout.Label ("", "SpikeMid");//-------------------------------- custom
	        }
		GUILayout.Label ("", "SpikeRight");//-------------------------------- custom
		GUILayout.EndHorizontal();
	}
	
	public static void FancyTop(float topX) {
		float leafOffset = (topX / 2) - 64;
		float frameOffset = (topX / 2) - 27;
		float skullOffset = (topX / 2) - 20;
		GUI.Label(new Rect(leafOffset, 18, 0, 0), "", "GoldLeaf");//-------------------------------- custom	
		GUI.Label(new Rect(frameOffset, 3, 0, 0), "", "IconFrame");//-------------------------------- custom	
		GUI.Label(new Rect(skullOffset, 12, 0, 0), "", "Skull");//-------------------------------- custom	
	}
	
	public static void WaxSeal(float x, float y) {
		float WSwaxOffsetX = x - 120;
		float WSwaxOffsetY = y - 115;
		float WSribbonOffsetX = x - 114;
		float WSribbonOffsetY = y - 83;
		
		GUI.Label(new Rect(WSribbonOffsetX, WSribbonOffsetY, 0, 0), "", "RibbonBlue");//-------------------------------- custom	
		GUI.Label(new Rect(WSwaxOffsetX, WSwaxOffsetY, 0, 0), "", "WaxSeal");//-------------------------------- custom	
	}
}
