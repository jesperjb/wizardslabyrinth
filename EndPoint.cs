using UnityEngine;
using System.Collections;

public class EndPoint : MonoBehaviour {
	
	void OnCollisionEnter(Collision collision) {
		GameObject other = collision.collider.gameObject;
    	if (other.gameObject.tag != "Player") {
			return;
		}
		SendMessageUpwards("GameEnded");
    }
}
