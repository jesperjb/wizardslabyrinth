using UnityEngine;
using System.Collections;

public class LevelController : MonoBehaviour {
	
	// Labyrinth start and end.
	public GameObject startPoint;
	public GameObject endPoint;
	// Game manager object
	private GameManager controller;
	
	// Game state
	private bool playerCompleted = false;
	private bool levelEnded = false;
	
	// Timer
	private float startTime;
	public float countDownSeconds = 120.0f;
	public bool timeStarted = false;
	private float restSeconds;
	
	// GUI
	public GUISkin gSkin;

	// Use this for initialization
	void Awake () {
		// Find endPoint if not set.
		if (!endPoint) {
			endPoint = GameObject.FindGameObjectWithTag("End");
		}
		// Find startPoint if not set.
		if(!startPoint) {
			startPoint = GameObject.FindGameObjectWithTag("Start");
		}
		if(!controller) {
			controller = GameObject.FindObjectOfType(typeof(GameManager)) as GameManager;
		}
			
		// Get the selected character
		string character = PlayerPrefs.GetString("SelectedCharacter");
		Debug.Log(character);
		// Instantiate the character prefab 
		MainCharacter player = Instantiate(Resources.Load("prefabs/Characters/" + character, typeof(MainCharacter))) as MainCharacter;
		// Set player name for niceness.
		player.name = "Player";
		// Set player position
		player.transform.position = startPoint.transform.position;
		// Parent player to the level controller so it can receive broadcasts.
		player.transform.parent = gameObject.transform;
		// Set camera target
		Camera camera = Camera.main;
		camera.GetComponent<MainCamera>().target = player.transform;
	}
	
	void Start () {
		// Set start time.
		startTime = Time.time;
	}
	
	/**
	 * Broadcast the end game message
	 */
	void GameEnded () {
		BroadcastMessage("EndGame", SendMessageOptions.DontRequireReceiver);
	}
	
	void OnGUI () {
		GUI.skin = gSkin;
		// If game has not ended display the countdown.
		if(!levelEnded) {
			// Get the current time as a formatted string.
		    string niceTime = GetCurrentTime();
			// Display the time on the screen.
		    GUI.Label(new Rect(Screen.width / 2 - 50, 20,100,30), niceTime);
		// Else display the time and a menu.
		} else {
			Rect areaRect = new Rect(Screen.width / 2 - 50, Screen.height / 2 - 150, 100, 300);
			GUILayout.BeginArea(areaRect);
			GUILayout.FlexibleSpace();
			// Display time.
			string niceTime = FormatTime(restSeconds);
			GUILayout.Label("Time: " + niceTime);
			// Display options.
			if(playerCompleted) {
				if(GUILayout.Button("Play next level")) {
					controller.LoadNextLevel();
				}
			} else {
				if(GUILayout.Button("Restart")) {
					controller.RestartLevel();
				}
			}
			GUILayout.FlexibleSpace();
	        GUILayout.EndArea();
		}
	}
	
	/**
	 * Countdown time and return it as a formated string.
	 */
	string GetCurrentTime() {
		//make sure that your time is based on when this script was first called
		//instead of when your game started
		float guiTime = Time.time - startTime;
		restSeconds = countDownSeconds - (guiTime);
		
		//display messages or whatever here -->do stuff based on your timer
		if (restSeconds == 60) {
			print("One Minute Left");
		}
		if (restSeconds == 0) {
			print("Time is Over");
			GameEnded();
		}
		return FormatTime(restSeconds);
	}
	
	string FormatTime(float time) {
		time = Mathf.CeilToInt(time);
	    int minutes = Mathf.FloorToInt(time / 60F);
	    int seconds = Mathf.FloorToInt(time - minutes * 60);
		return string.Format("{0:0}:{1:00}", minutes, seconds);
	}
	
	/**
	 * Perform end game functionality.
	 */
	void EndGame () {
		levelEnded = true;
		if (restSeconds == 0) {
			playerCompleted = false;
		} else {
			playerCompleted = true;
		}
	}
}
